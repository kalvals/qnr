package com.chase.ffg;

import lombok.*;

import javax.websocket.server.ServerEndpoint;

@Data@AllArgsConstructor@NoArgsConstructor@Getter@Setter
public class Cost {

    private String initialConsiderations;
    private String budgetConsiderations;
    private String vehicle;
    private String budget;
}
