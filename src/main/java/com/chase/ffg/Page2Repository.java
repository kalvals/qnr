package com.chase.ffg;

import org.springframework.data.repository.CrudRepository;

public interface Page2Repository extends CrudRepository<Page2,Long> {
}
