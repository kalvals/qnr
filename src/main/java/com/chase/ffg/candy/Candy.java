package com.chase.ffg.candy;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Entity
@Data
@NoArgsConstructor@AllArgsConstructor@Getter@Setter
public class Candy {

    @Id@GeneratedValue
    private Long id;

    private String candyColor;
    private String candyShape;
    private String candyPref;
    private String candyFilling;
}
