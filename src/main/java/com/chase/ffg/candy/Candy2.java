package com.chase.ffg.candy;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity@Data @NoArgsConstructor@AllArgsConstructor@Getter@Setter
public class Candy2 {

    @Id
    @GeneratedValue
    private Long id;

    private String name;
    private String location;
    private String restrictions;
    private String shape;
    private String candyFilling;
    private String store;
    private String count;
    private String type;
    private String lastBought;
    private String regularBasis;
    private String cost;
    private String bulk;
    private String outOfYourWay;

}
