package com.chase.ffg.candy;

import org.springframework.data.repository.CrudRepository;

public interface CandyRepository extends CrudRepository<Candy,Long> {
}
