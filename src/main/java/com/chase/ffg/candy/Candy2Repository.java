package com.chase.ffg.candy;

import org.springframework.data.repository.CrudRepository;

public interface Candy2Repository extends CrudRepository<Candy2,Long> {
}
