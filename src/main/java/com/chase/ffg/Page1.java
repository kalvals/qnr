package com.chase.ffg;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Entity
@Data
@Getter@Setter@AllArgsConstructor@NoArgsConstructor
public class Page1 {

    @Id
    @GeneratedValue
    private Long id;

    private String ngoValue;
    private String scale;
    private String regions;
    private String size;
    private String services;

}
