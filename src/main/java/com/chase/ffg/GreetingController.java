package com.chase.ffg;

import com.chase.ffg.Greeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GreetingController {

    @Autowired
    private Page1Repository page1Repository;

    @Autowired
    private Page2Repository page2Repository;

    @GetMapping("/")
    public String greetingForm(Model model) {
        //model.addAttribute("page1", new Page1());
        return "greeting";
    }

    @PostMapping("/saveGreeting")
    public String saveGreeting(Model model){
        model.addAttribute("page1", new Page1());
        return "page1";
    }

    @PostMapping("/savePage1")
    public String greetingSubmit(@ModelAttribute Page1 page1, Model model) {
        page1Repository.save(page1);
        model.addAttribute("page2",new Page2());
        return "page2";
    }

    @PostMapping("/savePage2")
    public String page2(@ModelAttribute Page2 page2, Model model) {
        page2Repository.save(page2);
        model.addAttribute("platform", new Platform());
        return "value";
    }

    @PostMapping("/savePlatform")
    public String savePlatform(@ModelAttribute Platform platform, Model model) {
        //page2Repository.save(Platform);
        model.addAttribute("cost", new Cost());
        return "cost";
    }

    @PostMapping("/saveCost")
    public String saveCost(@ModelAttribute Platform platform, Model model) {
        //page2Repository.save(Platform);
        //model.addAttribute("cost", new Cost());
        return "result";
    }

}