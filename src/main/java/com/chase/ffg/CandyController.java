package com.chase.ffg;

import com.chase.ffg.candy.Candy;
import com.chase.ffg.candy.Candy2;
import com.chase.ffg.candy.Candy2Repository;
import com.chase.ffg.candy.CandyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class CandyController {

    @Autowired
    private CandyRepository candyRepository;

    @Autowired
    private Candy2Repository candy2Repository;

    @GetMapping("/candy")
    public String greetingForm(Model model) {
        //model.addAttribute("page1", new Page1());
        return "candy";
    }

    @GetMapping("/candy2")
    public String getCandy2(Model model) {
        //model.addAttribute("page1", new Page1());
        return "candy2";
    }

    @PostMapping("/saveCandy")
    public String saveCandy(@ModelAttribute Candy candy, Model model){
        candyRepository.save(candy);
        if(candy.getCandyColor().equals("dark") && candy.getCandyShape().equals("hard")){
            model.addAttribute("result","Hersheys!");
        }else{
            model.addAttribute("result","Kisses!");
        }
        return "result";

    }

    @PostMapping("/saveCandy2")
    public String saveCandy2(@ModelAttribute Candy2 candy2, Model model){
        candy2Repository.save(candy2);
        if(candy2.getShape().equals("dark") && candy2.getBulk().equals("none")){
            model.addAttribute("result","Hersheys!");
        }else{
            model.addAttribute("result","Kisses!");
        }
        return "candyResult";

    }
}
