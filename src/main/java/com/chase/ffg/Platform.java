package com.chase.ffg;

import lombok.*;
import org.hibernate.annotations.GeneratorType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
@Data@NoArgsConstructor@AllArgsConstructor@Getter@Setter
@Entity
public class Platform {
@Id@GeneratedValue
    private Long id;
    private String businessValue;
    private String features;
    private String keySuccessCriteria;


}