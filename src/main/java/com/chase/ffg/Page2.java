package com.chase.ffg;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@AllArgsConstructor
@Getter@Setter
@NoArgsConstructor

public  class Page2 {
@Id@GeneratedValue
    private Long id;
    private String busProcs;
    private String repeatableProcs;
    private String automateCandidates;
    private String beneficiaries;


}
